package com.user.service;

import com.user.pojo.vo.order.BaseOrderVO;

public interface OrderService {
    void placeOrder(BaseOrderVO baseOrderVO);
}
