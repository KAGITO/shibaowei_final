package com.user.service;

import com.user.constant.Constant;
import com.user.dao.InstanceMapper;
import com.user.dao.OrderMapper;
import com.user.pojo.db.InstanceDO;
import com.user.pojo.db.OrderDO;
import com.user.pojo.vo.order.OrderVO;
import com.user.utils.CopyUtil;
import org.springframework.stereotype.Service;
import com.user.utils.IdGenerator;
import com.user.pojo.vo.order.BaseOrderVO;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;
    @Resource
    private InstanceMapper instanceMapper;

    public void placeOrder(BaseOrderVO baseOrderVO) {
        //订单
        OrderDO orderDO = new OrderDO();
        long orderId = IdGenerator.nextId();
        orderDO.setOrderId(orderId);
        orderDO.setUserId(Long.parseLong(baseOrderVO.getUserId()));
        //前端算好的价格
        orderDO.setTotalPrice(baseOrderVO.getTotalPrice());
        orderDO.setOrderState(Constant.OrderState.PENDING_REVIEW);
        Timestamp curTime = Timestamp.valueOf(LocalDateTime.now());
        orderDO.setCreateTime(curTime);
        orderDO.setLastEditTime(curTime);
        //保存
        orderMapper.addOrder(orderDO);
        //实例
        List<InstanceDO> instances = new ArrayList<>();
        for (String productId : baseOrderVO.getProductIds()) {
            InstanceDO instanceDO = new InstanceDO();
            instanceDO.setInstanceId(IdGenerator.nextId());
            instanceDO.setOrderId(orderId);
            instanceDO.setProductId(Long.parseLong(productId));
            instanceDO.setInstanceState(Constant.InstanceState.INEFFECTIVE);
            instanceDO.setCreateTime(curTime);
            instances.add(instanceDO);
        }
        //保存
        instanceMapper.addBatchInstances(instances);
    }
}
