package com.user.controller;

import com.user.constant.Constant;
import com.user.pojo.vo.order.OrderVO;
import com.user.service.CommonOrderService;
import com.user.utils.HttpServletRequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.user.service.OrderService;
import com.user.pojo.vo.common.ResponseVO;
import com.user.pojo.vo.order.BaseOrderVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private CommonOrderService commonOrderService;

    @PostMapping("/order")
    public ResponseVO placeOrder(@RequestBody BaseOrderVO baseOrderVO) {
        orderService.placeOrder(baseOrderVO);
        return ResponseVO.success("下单成功");
    }

    @GetMapping("/my_unreviewed_order")
    public ResponseVO getMyUnreviewedOrders(HttpServletRequest request) {
        String userId = HttpServletRequestUtil.getString(request, "userId");
        List<OrderVO> orderVOs = commonOrderService.getSomeonesOrderByState(userId, Constant.OrderState.PENDING_REVIEW);
        return ResponseVO.success(orderVOs);
    }

    @GetMapping("/unpaid_order")
    public ResponseVO getUnpaidOrders(HttpServletRequest request) {
        String userId = HttpServletRequestUtil.getString(request, "userId");
        List<OrderVO> orderVOs = commonOrderService.getSomeonesOrderByState(userId, Constant.OrderState.NO_PAYMENT);
        return ResponseVO.success(orderVOs);
    }

    @PostMapping("/paid_order/{orderId}")
    public ResponseVO pay(@PathVariable("orderId") String orderId) {
        commonOrderService.pay(orderId);
        return ResponseVO.success("支付成功");
    }

    @GetMapping("/paid_order")
    public ResponseVO getPaidOrders(HttpServletRequest request) {
        String userId = HttpServletRequestUtil.getString(request, "userId");
        List<OrderVO> orderVOs = commonOrderService.getSomeonesOrderByState(userId, Constant.OrderState.PAID);
        return ResponseVO.success(orderVOs);
    }
}
