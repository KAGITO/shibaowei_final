-- 产品 --
DROP TABLE IF EXISTS `cm_product`;
CREATE TABLE `cm_product`
(
    `product_id`     int(100) NOT NULL AUTO_INCREMENT primary key,
    `product_name`   varchar(100) NOT NULL,
    `product_desc`   varchar(2000) DEFAULT NULL,
    `img_addr`       varchar(2000) DEFAULT '',
    `price`          varchar(100)  DEFAULT NULL,
    `validity`       int(3) DEFAULT 0,
    `create_time`    datetime      DEFAULT NULL,
    `last_edit_time` datetime      DEFAULT NULL,
    `enable_status`  int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
-- 订单 --
DROP TABLE IF EXISTS `cm_order`;
CREATE TABLE `cm_order`
(
    `order_id`       BIGINT UNSIGNED PRIMARY KEY,
    `user_id`        BIGINT UNSIGNED NOT NULL,
    `order_state`    int(2) NOT NULL DEFAULT '1',
    `price`          varchar(100) DEFAULT NULL,
    `reviewer`       varchar(100) DEFAULT NULL,
    `create_time`    datetime     DEFAULT NULL,
    `last_edit_time` datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 实例 ——
DROP TABLE IF EXISTS `cm_instance`;
CREATE TABLE `cm_instance`
(
    `instance_id`    BIGINT UNSIGNED PRIMARY KEY,
    `order_id`       BIGINT UNSIGNED NOT NULL,
    `product_id`     BIGINT UNSIGNED NOT NULL,
    `instance_state` int(2) NOT NULL DEFAULT '1',
    `create_time`    datetime DEFAULT NULL,
    `effective_time` datetime DEFAULT NULL,
    `expire_time`    datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
--user
DROP TABLE IF EXISTS `cm_user`;
CREATE TABLE `cm_user`
(
    `user_id`      bigint UNSIGNED not null primary key,
    `name`         varchar(50) not null unique key,
    `phone_number` varchar(12) not null unique key,
    `password`     char(32)    not null,
    `login_time`   datetime DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
--role
DROP TABLE IF EXISTS `cm_role`;
CREATE TABLE `cm_role`
(
    `role_id` bigint UNSIGNED not null primary key,
    `user_id` bigint UNSIGNED not null,
    `role`    int(2) NOT NULL DEFAULT 1
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO shibaowei.cm_product (product_id, product_name, product_desc, img_addr, price, validity, create_time, last_edit_time, enable_status) VALUES (1, 'CMC宽带', '标准资费每月按120元收取，可以根据各地区业务规定进行优惠，宽带的上下行速率均为50M', '/user/shibawei', '120', 1, null, null, 1);
INSERT INTO shibaowei.cm_product (product_id, product_name, product_desc, img_addr, price, validity, create_time, last_edit_time, enable_status) VALUES (16, 'CMC名片', 'CMC名片 手机用户拨打客户固定电话，在通话结束后，将收到预先设置好的短信，短信内容可以是产品介绍、促销活动信息等', '/user/shibawei', '120', 2, null, null, 1);
INSERT INTO shibaowei.cm_product (product_id, product_name, product_desc, img_addr, price, validity, create_time, last_edit_time, enable_status) VALUES (17, 'CMC彩铃', ' 要求在规定的时间内将用户铃音作为用户的默认铃音', '/user/shibawei', '120', 2, null, null, 1);