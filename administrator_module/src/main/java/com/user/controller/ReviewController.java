package com.user.controller;

import com.user.constant.Constant;
import com.user.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.user.pojo.vo.order.OrderVO;
import com.user.pojo.vo.common.ResponseVO;
import com.user.service.CommonOrderService;

import java.util.List;

@RestController
public class ReviewController {
    @Autowired
    private CommonOrderService orderService;
    @Autowired
    private ReviewService reviewService;

    /**
     * 查看未审批的订单
     *
     * @return
     */
    @GetMapping("/unreviewed_order")
    public ResponseVO getUnreviewedOrders() {
        List<OrderVO> orders = orderService.getOrdersByState(Constant.OrderState.PENDING_REVIEW);
        return ResponseVO.success(orders);
    }

    /**
     * 获取一个未审批的订单
     *
     * @param orderId
     * @return
     */
    @GetMapping("/unreviewed_order/{orderId}")
    public ResponseVO getUnreviewedOrderById(@PathVariable("orderId") String orderId) {
        OrderVO order = orderService.getOrderByOrderId(Long.parseLong(orderId));
        return ResponseVO.success(order);
    }

    /**
     * 将一个订单置为待审批
     *
     * @param orderId
     * @param reviewer
     * @return
     */
    @PostMapping("/reviewing_order/{orderId}")
    public ResponseVO choose(@PathVariable("orderId") String orderId, @RequestBody String reviewer) {
        reviewService.addReviewer(orderId, reviewer);
        return ResponseVO.success("待审批操作成功");
    }

    /**
     * 审批一个订单
     *
     * @param orderId
     * @return
     */
    @PostMapping("/reviewed_order/{orderId}")
    public ResponseVO review(@PathVariable("orderId") String orderId) {
        reviewService.review(orderId);
        return ResponseVO.success("审批成功");
    }

    @PostMapping("/rejected_order/{orderId}")
    public ResponseVO reject(@PathVariable("orderId") String orderId) {
        reviewService.review(orderId);
        return ResponseVO.success("审批成功");
    }
}
