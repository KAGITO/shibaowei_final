package com.user.controller;

import com.user.pojo.vo.common.ResponseVO;
import com.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping("role_cache")
    public ResponseVO addRolesToCache() {
        roleService.addRolesToCache();
        return ResponseVO.success("权限刷新成功");
    }
}
