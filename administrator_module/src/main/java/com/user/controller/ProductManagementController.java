package com.user.controller;


import com.user.pojo.dto.ImageHolder;
import com.user.pojo.vo.product.ProductAdditionVO;
import com.user.pojo.vo.product.ProductVO;
import com.user.service.ProductManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.user.pojo.vo.common.ResponseVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RestController
@Slf4j
public class ProductManagementController {
    @Autowired
    private ProductManagementService productManagementService;

    /**
     * 添加产品
     *
     * @return
     */
    @PostMapping("/product")
    public ResponseVO addProduct(@RequestBody ProductAdditionVO productAdditionVO) {
        productManagementService.addProduct(productAdditionVO);
        return ResponseVO.success("产品添加成功");
    }

    @DeleteMapping("/product/{productId}")
    public ResponseVO deleteProductById(@PathVariable("productId") String productId) {
        productManagementService.deleteProductById(productId);
        return ResponseVO.success("产品删除成功");
    }

    /**
     * 刷产品缓存
     *
     * @return
     */
    @PostMapping("/product_cache")
    public ResponseVO addProductDataToCache() {
        productManagementService.addProductsToCache();
        return ResponseVO.success("缓存刷新成功");
    }

    @PostMapping("/product_img")
    public ResponseVO uploadImg(MultipartFile file) {
        ImageHolder thumbnail = null;
        try {
            thumbnail = handleImage(file, thumbnail);
        } catch (IOException e) {
            return ResponseVO.success("产品添加异常");
        }
        String imgAddr = productManagementService.uploadImg(thumbnail);
        Map<String, String> map = new HashMap<>();
        map.put("imgAddr", imgAddr);
        return ResponseVO.success(map);
    }

    /**
     * 图片预处理
     */
    private ImageHolder handleImage(MultipartFile file, ImageHolder thumbnail)
            throws IOException {
        if (file != null) {
            thumbnail = new ImageHolder(file.getOriginalFilename(), file.getInputStream());
        }
        return thumbnail;
    }
}
