package com.user.controller;

import com.user.pojo.vo.common.ResponseVO;
import com.user.pojo.vo.user.UserVO;
import com.user.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserManagementController {
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("/stable_user")
    public ResponseVO getStableUsers() {
        List<UserVO> userVOs = userManagementService.getStableUsers();
        return ResponseVO.success(userVOs);
    }

    @GetMapping("/inactive_user")
    public ResponseVO getInactiveUsers() {
        List<UserVO> userVOs = userManagementService.getInactiveUsers();
        return ResponseVO.success(userVOs);
    }

    @GetMapping("/lost_user")
    public ResponseVO getLostUsers() {
        List<UserVO> userVOs = userManagementService.getLostUsers();
        return ResponseVO.success(userVOs);
    }

    @DeleteMapping("/user/{userId}")
    public ResponseVO deleteUserById(@PathVariable("userId") String userId) {
        userManagementService.deleteUserById(userId);
        return ResponseVO.success("删除成功");
    }
}
