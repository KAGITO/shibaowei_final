package com.user.service;

import com.user.constant.Constant;
import com.user.dao.RoleMapper;
import com.user.pojo.db.RoleDO;
import com.user.utils.RedisService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RedisService redisService;
    @Resource
    private RoleMapper roleMapper;

    @Override
    public void addRolesToCache() {
        redisService.delete(Constant.Role.CACHE);
        List<RoleDO> roleDOs = roleMapper.selectRoles();
        if (roleDOs == null || roleDOs.size() == 0) return;
        for (RoleDO roleDO : roleDOs) {
            redisService.hashPut(Constant.Role.CACHE, roleDO.getUserId(), String.valueOf(roleDO.getRole()));
        }
    }
}
