package com.user.service;

import com.user.constant.Constant;
import com.user.dao.OrderMapper;
import com.user.pojo.db.OrderDO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Resource
    private OrderMapper orderMapper;

    @Override
    public void addReviewer(String orderId, String reviewer) {
        OrderDO orderDO = new OrderDO();
        orderDO.setOrderId(Long.parseLong(orderId));
        orderDO.setReviewer(reviewer);
        orderDO.setOrderState(Constant.OrderState.REVIEWING);
        orderDO.setLastEditTime(Timestamp.valueOf(LocalDateTime.now()));
        orderMapper.updateOrder(orderDO);
    }

    @Override
    public void review(String orderId) {
        OrderDO orderDO = new OrderDO();
        orderDO.setOrderId(Long.parseLong(orderId));
        orderDO.setOrderState(Constant.OrderState.NO_PAYMENT);
        orderDO.setLastEditTime(Timestamp.valueOf(LocalDateTime.now()));
        orderMapper.updateOrder(orderDO);
    }

    @Override
    public void reject(String orderId) {
        OrderDO orderDO = new OrderDO();
        orderDO.setOrderId(Long.parseLong(orderId));
        orderDO.setOrderState(Constant.OrderState.NO_APPROVED);
        orderDO.setLastEditTime(Timestamp.valueOf(LocalDateTime.now()));
        orderMapper.updateOrder(orderDO);
    }
}
