package com.user.service;

import com.user.dao.UserMapper;
import com.user.pojo.db.UserDO;
import com.user.pojo.vo.user.UserVO;
import com.user.utils.CopyUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserManagementServiceImpl implements UserManagementService {
    @Resource
    private UserMapper userMapper;

    @Override
    public List<UserVO> getStableUsers() {
        List<UserDO> userDOs = userMapper.selectUserLoginTimeLessThan(1);
        return CopyUtil.copyList(userDOs, UserVO.class);
    }
    
    @Override
    public List<UserVO> getInactiveUsers() {
        List<UserDO> userDOs = userMapper.selectUserLoginTimeBetween(1, 2);
        return CopyUtil.copyList(userDOs, UserVO.class);
    }

    @Override
    public List<UserVO> getLostUsers() {
        List<UserDO> userDOs = userMapper.selectUserLoginTimeMoreThan(3);
        return CopyUtil.copyList(userDOs, UserVO.class);
    }

    @Override
    public void deleteUserById(String userId) {
        userMapper.deleteUserById(Long.parseLong(userId));
    }
}
