package com.user.service;

import com.user.constant.Constant;
import com.user.dao.ProductMapper;
import com.user.pojo.db.ProductDO;
import com.user.pojo.dto.ImageHolder;
import com.user.pojo.vo.product.ProductAdditionVO;
import com.user.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductManagementManagementServiceImpl implements ProductManagementService {

    @Resource
    private ProductMapper productMapper;
    @Autowired
    private RedisService redisService;

    @Override
    public void addProductsToCache() {
        redisService.delete(Constant.Product.PRODUCT);
        List<ProductDO> productDOs = productMapper.getProductsByState(Constant.ProductState.VALID);
        for (ProductDO productDO : productDOs) {
            redisService.hashPut(String.valueOf(productDO.getProductId()), "productName", productDO.getProductName());
            redisService.hashPut(String.valueOf(productDO.getProductId()), "productDesc", productDO.getProductDesc());
            redisService.hashPut(String.valueOf(productDO.getProductId()), "imgAddr", productDO.getImgAddr());
            redisService.hashPut(String.valueOf(productDO.getProductId()), "price", productDO.getPrice());
            redisService.hashPut(String.valueOf(productDO.getProductId()), "validity", String.valueOf(productDO.getValidity()));
            redisService.listPush(Constant.Product.PRODUCT, String.valueOf(productDO.getProductId()));
        }
    }

    @Override
    public void addProduct(ProductAdditionVO productAdditionVO) {
        ProductDO productDO = CopyUtil.copy(productAdditionVO, ProductDO.class);
        productMapper.addProduct(productDO);
    }


    private String addThumbnail(ImageHolder thumbnail) {
        String dest = PathUtil.getImgBasePath();
        return ImageUtil.generateThumbnail(thumbnail, dest);
    }

    @Override
    public void deleteProductById(String productId) {
        productMapper.deleteProductById(productId);
        redisService.delete(productId);
    }

    @Override
    public String uploadImg(ImageHolder thumbnail) {
        return addThumbnail(thumbnail);
    }
}
