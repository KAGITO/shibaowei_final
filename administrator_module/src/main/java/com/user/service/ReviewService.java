package com.user.service;

public interface ReviewService {
    void addReviewer(String orderId, String reviewer);
    void review(String orderId);
    void reject(String orderId);
}
