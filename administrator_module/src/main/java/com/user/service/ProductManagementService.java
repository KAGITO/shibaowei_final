package com.user.service;

import com.user.pojo.dto.ImageHolder;
import com.user.pojo.vo.product.ProductAdditionVO;
import com.user.pojo.vo.product.ProductVO;

public interface ProductManagementService {
    void addProductsToCache();

    void addProduct(ProductAdditionVO productAdditionVO);

    void deleteProductById(String productId);

    String uploadImg(ImageHolder thumbnail);
}
