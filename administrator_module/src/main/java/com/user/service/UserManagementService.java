package com.user.service;

import com.user.pojo.vo.user.UserVO;

import java.util.List;

public interface UserManagementService {
    List<UserVO> getStableUsers();

    List<UserVO> getInactiveUsers();

    List<UserVO> getLostUsers();

    void deleteUserById(String userId);
}
