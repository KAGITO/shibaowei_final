package com.user.service;

import com.user.pojo.vo.order.OrderVO;

import java.util.List;

public interface CommonOrderService {
    List<OrderVO> getOrdersByState(int orderState);

    OrderVO getOrderByOrderId(Long orderId);

    List<OrderVO> getSomeonesOrderByState(String userId, int orderState);

    void pay(String orderId);
}
