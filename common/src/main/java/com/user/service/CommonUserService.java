package com.user.service;

import com.user.pojo.vo.login.UserLoginReqVO;
import com.user.pojo.vo.login.UserLoginRespVO;
import com.user.pojo.vo.user.UserSaveVO;

public interface CommonUserService {
    void register(UserSaveVO userSaveVO);

    UserLoginRespVO login(UserLoginReqVO userLoginReqVO);
}
