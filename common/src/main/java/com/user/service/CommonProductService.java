package com.user.service;

import com.user.pojo.vo.product.ProductVO;

import java.util.List;

public interface CommonProductService {
    List<ProductVO> getProducts();

    List<ProductVO> getProductsByKeyword(String keyword);
}
