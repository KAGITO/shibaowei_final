package com.user.service;

import com.alibaba.fastjson.JSONObject;
import com.user.constant.Constant;
import com.user.dao.UserMapper;
import com.user.pojo.db.UserDO;
import com.user.pojo.vo.login.UserLoginReqVO;
import com.user.pojo.vo.login.UserLoginRespVO;
import com.user.pojo.vo.user.UserSaveVO;
import com.user.utils.CopyUtil;
import com.user.utils.IdGenerator;
import com.user.utils.LoginUserContext;
import com.user.utils.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class CommonUserServiceImpl implements CommonUserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private RedisService redisService;

    public void register(UserSaveVO userSaveVO) {
        UserDO userDO = CopyUtil.copy(userSaveVO, UserDO.class);
        userDO.setUserId(IdGenerator.nextId());
        userDO.setPassword(DigestUtils.md5DigestAsHex(userSaveVO.getPassword().getBytes()));
        userDO.setLoginTime(Timestamp.valueOf(LocalDateTime.now()));
        userMapper.addUser(userDO);
    }

    @Override
    public UserLoginRespVO login(UserLoginReqVO userLoginReqVO) {
        UserDO userDO = userMapper.selectUserByPhoneNumber(userLoginReqVO.getPhoneNumber());
        if (userDO == null)
            throw new RuntimeException("用户不存在");
        if (!DigestUtils.md5DigestAsHex(userLoginReqVO.getPassword().getBytes()).equals(userDO.getPassword()))
            throw new RuntimeException("密码错误");
        Long token = IdGenerator.nextId();
        UserLoginRespVO resp = new UserLoginRespVO();
        resp.setName(userDO.getName());
        resp.setUserId(userDO.getUserId());
        resp.setPhoneNumber(userDO.getPhoneNumber());
        resp.setToken(String.valueOf(token));
        Map<String, String> users = redisService.bashHashGet(Constant.Role.CACHE);
        String roleStr = users.get(String.valueOf(userDO.getUserId()));
        resp.setRole(Integer.parseInt(roleStr));
        redisService.putNormalValueWithExpiration(token.toString(), JSONObject.toJSONString(resp), 3600 * 24, TimeUnit.SECONDS);
        UserDO updatedUser = new UserDO();
        updatedUser.setUserId(userDO.getUserId());
        updatedUser.setLoginTime(Timestamp.valueOf(LocalDateTime.now()));
        userMapper.updateUser(updatedUser);
        return resp;
    }
}
