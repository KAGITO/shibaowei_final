package com.user.service;

import com.user.constant.Constant;
import com.user.dao.InstanceMapper;
import com.user.dao.OrderMapper;
import com.user.pojo.db.InstanceDO;
import com.user.pojo.vo.product.ProductVO;
import com.user.utils.CopyUtil;
import com.user.utils.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.user.pojo.db.OrderDO;
import com.user.pojo.vo.order.OrderVO;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CommonOrderServiceImpl implements CommonOrderService {
    @Resource
    private OrderMapper orderMapper;
    @Autowired
    private RedisService redisService;
    @Resource
    private InstanceMapper instanceMapper;

    @Override
    public List<OrderVO> getOrdersByState(int orderState) {
        List<OrderDO> orderDOs = orderMapper.getOrdersByState(orderState);
        List<OrderVO> orders = new ArrayList<>();
        for (OrderDO orderDO : orderDOs) {
            OrderVO order = CopyUtil.copy(orderDO, OrderVO.class);
            order.setUserName(orderDO.getUser().getName());
            List<InstanceDO> instanceDOs = orderDO.getInstances();
            List<ProductVO> products = new ArrayList<>();
            for (InstanceDO instanceDO : instanceDOs) {
                Long productId = instanceDO.getProductId();
                Map<String, String> productMap = redisService.bashHashGet(String.valueOf(productId));
                ProductVO product = new ProductVO();
                product.setProductId(productId);
                product.setProductName(productMap.get("productName"));
                product.setProductDesc(productMap.get("productDesc"));
                product.setImgAddr(productMap.get("imgAddr"));
                product.setPrice(productMap.get("price"));
                product.setValidity(productMap.get("validity"));
                products.add(product);
            }
            order.setProducts(products);
            orders.add(order);
        }
        return orders;
    }

    @Override
    public OrderVO getOrderByOrderId(Long orderId) {
        OrderDO orderDO = orderMapper.getOrderByOrderId(orderId);
        return CopyUtil.copy(orderDO, OrderVO.class);
    }

    @Override
    public List<OrderVO> getSomeonesOrderByState(String userId, int orderState) {
        List<OrderDO> orderDOs = orderMapper.getSomeonesOrdersByState(userId, orderState);
        List<OrderVO> orders = new ArrayList<>();
        for (OrderDO orderDO : orderDOs) {
            OrderVO order = CopyUtil.copy(orderDO, OrderVO.class);
            order.setUserName(orderDO.getUser().getName());
            List<InstanceDO> instanceDOs = orderDO.getInstances();
            List<ProductVO> products = new ArrayList<>();
            for (InstanceDO instanceDO : instanceDOs) {
                Long productId = instanceDO.getProductId();
                Map<String, String> productMap = redisService.bashHashGet(String.valueOf(productId));
                ProductVO product = new ProductVO();
                product.setProductId(productId);
                product.setProductName(productMap.get("productName"));
                product.setProductDesc(productMap.get("productDesc"));
                product.setImgAddr(productMap.get("imgAddr"));
                product.setPrice(productMap.get("price"));
                product.setValidity(productMap.get("validity"));
                products.add(product);
            }
            order.setProducts(products);
            orders.add(order);
        }
        return orders;
    }

    @Override
    public void pay(String orderId) {
        OrderDO updatedOrderDO = new OrderDO();
        updatedOrderDO.setOrderId(Long.parseLong(orderId));
        updatedOrderDO.setOrderState(Constant.OrderState.PAID);
        updatedOrderDO.setLastEditTime(Timestamp.valueOf(LocalDateTime.now()));
        orderMapper.updateOrder(updatedOrderDO);
        //生效 fast-fail?????
        List<InstanceDO> InstanceDOs = instanceMapper.selectInstancesByOrderId(orderId);
        for (InstanceDO instanceDO : InstanceDOs) {
            Map<String, String> productMap = redisService.bashHashGet(String.valueOf(instanceDO.getProductId()));
            LocalDateTime cur = LocalDateTime.now();
            instanceDO.setEffectiveTime(Timestamp.valueOf(cur));
            LocalDateTime expire = cur.plusDays(Long.parseLong(productMap.get("validity")));
            instanceDO.setExpireTime(Timestamp.valueOf(expire));
            instanceDO.setInstanceState(Constant.InstanceState.EFFECTIVE);
        }
        instanceMapper.updateBatchInstances(InstanceDOs);
    }
}
