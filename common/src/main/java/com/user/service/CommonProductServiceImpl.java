package com.user.service;

import com.user.constant.Constant;
import com.user.pojo.vo.product.ProductVO;
import com.user.utils.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CommonProductServiceImpl implements CommonProductService {
    @Autowired
    private RedisService redisService;

    @Override
    public List<ProductVO> getProducts() {
        List<String> productIds = redisService.popAll(Constant.Product.PRODUCT);
        List<ProductVO> products = new ArrayList<>();
        for (String productId : productIds) {
            ProductVO product = new ProductVO();
            Map<String, String> productMap = redisService.bashHashGet(String.valueOf(productId));
            product.setProductId(Long.parseLong(productId));
            product.setProductName(productMap.get("productName"));
            product.setProductDesc(productMap.get("productDesc"));
            product.setImgAddr(productMap.get("imgAddr"));
            product.setPrice(productMap.get("price"));
            product.setValidity(productMap.get("validity"));
            products.add(product);
        }
        return products;
    }

    @Override
    public List<ProductVO> getProductsByKeyword(String keyword) {
        List<String> productIds = redisService.popAll(Constant.Product.PRODUCT);
        List<ProductVO> products = new ArrayList<>();
        for (String productId : productIds) {
            ProductVO product = new ProductVO();
            Map<String, String> productMap = redisService.bashHashGet(String.valueOf(productId));
            if (productMap.get("productName").contains(keyword)) {
                product.setProductId(Long.parseLong(productId));
                product.setProductName(productMap.get("productName"));
                product.setProductDesc(productMap.get("productDesc"));
                product.setImgAddr(productMap.get("imgAddr"));
                product.setPrice(productMap.get("price"));
                product.setValidity(productMap.get("validity"));
                products.add(product);
            }
        }
        return products;
    }
}
