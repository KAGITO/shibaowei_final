package com.user.controller;

import com.user.pojo.vo.common.ResponseVO;
import com.user.pojo.vo.login.UserLoginReqVO;
import com.user.pojo.vo.login.UserLoginRespVO;
import com.user.pojo.vo.user.UserSaveVO;
import com.user.service.CommonUserService;
import com.user.utils.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class CommonUserController {
    @Autowired
    private CommonUserService commonUserService;
    @Resource
    private RedisService redisService;

    @PostMapping("/register")
    public ResponseVO register(@RequestBody UserSaveVO userSaveVO) {
        commonUserService.register(userSaveVO);
        return ResponseVO.success("注册成功");
    }

    @PostMapping("/login")
    public ResponseVO login(@RequestBody UserLoginReqVO userLoginReqVO) {
        UserLoginRespVO userLoginRespVO = commonUserService.login(userLoginReqVO);
        return ResponseVO.success(userLoginRespVO);
    }

    @GetMapping("/logout/{token}")
    public ResponseVO logout(@PathVariable String token) {
        redisService.delete(token);
        return ResponseVO.success("已退出登录");
    }
}
