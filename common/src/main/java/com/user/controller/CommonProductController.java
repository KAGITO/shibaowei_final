package com.user.controller;

import com.user.pojo.vo.common.ResponseVO;
import com.user.pojo.vo.product.ProductQueryVO;
import com.user.pojo.vo.product.ProductVO;
import com.user.service.CommonProductService;
import com.user.utils.HttpServletRequestUtil;
import com.user.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CommonProductController {
    @Autowired
    private CommonProductService commonProductService;

    @GetMapping("/product")
    public ResponseVO getProducts(HttpServletRequest request) {
        String keyword = HttpServletRequestUtil.getString(request, "keyword");
        String pageNum = HttpServletRequestUtil.getString(request, "pagenum");
        String pageSize = HttpServletRequestUtil.getString(request, "pagesize");
        List<ProductVO> productVOs = null;
        if (request != null && !StringUtils.isEmpty(keyword)) {
            productVOs = commonProductService.getProductsByKeyword(keyword);
        } else {
            productVOs = commonProductService.getProducts();
        }
        List<ProductVO> resultProducts = PageUtil.startPage(productVOs, Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        Map<String, Object> result = new HashMap<>();
        result.put("total", productVOs.size());
        result.put("products", resultProducts);
        return ResponseVO.success(result);
    }
}


