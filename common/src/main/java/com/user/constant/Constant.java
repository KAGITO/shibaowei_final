package com.user.constant;

public class Constant {
    public interface OrderState {
        int PENDING_REVIEW = 1;//待审核
        int REVIEWING = 2;//审核中
        int NO_PAYMENT = 3;//待支付
        int PAID = 4;//支付完成
        int NO_APPROVED = 5;//审核不通过
        int WITH_EXCEPTION = 6;//失败
    }

    public interface InstanceState {
        int INEFFECTIVE = 1;//未生效
        int EFFECTIVE = 2;//生效中
        int INVALID = 3;//失效
    }

    public interface ProductState {
        int VALID = 1;
        int INVALID = 2;
    }

    public interface Role {
        String CACHE = "roles";
    }

    public interface Product {
        String PRODUCT = "products";
    }
}
