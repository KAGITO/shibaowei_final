package com.user.dao;

import com.user.pojo.db.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    int addUser(UserDO userDO);

    UserDO selectUserByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    int updateUser(UserDO userDO);

    int deleteUserById(@Param("userId") Long userId);

    List<UserDO> selectUserLoginTimeLessThan(@Param("monthNum") int monthNum);

    List<UserDO> selectUserLoginTimeBetween(@Param("minMonthNum") int minMonthNum, @Param("maxMonthNum") int MaxMonthNum);

    List<UserDO> selectUserLoginTimeMoreThan(@Param("monthNum") int monthNum);
}
