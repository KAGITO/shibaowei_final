package com.user.dao;


import com.user.pojo.db.RoleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {
    List<RoleDO> selectRoles();
}
