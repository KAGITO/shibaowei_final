package com.user.dao;

import com.user.pojo.db.OrderDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {
    int addOrder(OrderDO order);

    List<OrderDO> getOrdersByState(@Param("state") int state);

    List<OrderDO> getSomeonesOrdersByState(@Param("userId") String userId, @Param("state") int state);

    OrderDO getOrderByOrderId(@Param("orderId") Long orderId);

    int updateOrder(OrderDO order);
}
