package com.user.dao;

import com.user.pojo.db.InstanceDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface InstanceMapper {
    int addBatchInstances(@Param("instances") List<InstanceDO> instances);

    List<InstanceDO> selectInstancesByOrderId(@Param("orderId") String orderId);

    int updateBatchInstances(@Param("instances") List<InstanceDO> instances);
}
