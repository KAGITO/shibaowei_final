package com.user.dao;

import com.user.pojo.db.ProductDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProductMapper {
    List<ProductDO> getProductsByState(@Param("state") int state);

    int addProduct(ProductDO productDO);

    int deleteProductById(@Param("productId")String productId);
}
