package com.user.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class RedisService {
    @Autowired
    private StringRedisTemplate redisTemplate;

    public void hashPut(String key, String hashKey, String hashValue) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key, hashKey, hashValue);
    }

    public String hashGet(String key, String hashKey) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        return hashOperations.get(key, hashKey);
    }

    public Map<String, String> bashHashGet(String key) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        return hashOperations.entries(key);
    }

    public void listPush(String key, String value) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        listOperations.rightPush(key, value);
    }

    public String listIndex(String key, long index) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        return listOperations.index(key, index);
    }

    public Long listSize(String key) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        return listOperations.size(key);
    }


    public List<String> popAll(String key) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        return redisTemplate.boundListOps(key).range(0, listSize(key) - 1);
    }


    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public void putNormalValueWithExpiration(String key, String value, long time, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
    }

    public Object getValueByKey(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }
}