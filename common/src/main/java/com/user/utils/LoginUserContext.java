package com.user.utils;

import com.user.pojo.vo.login.UserLoginRespVO;

public class LoginUserContext {
    private static ThreadLocal<UserLoginRespVO> user = new ThreadLocal<>();

    public static UserLoginRespVO getUser() {
        return user.get();
    }

    public static void setUser(UserLoginRespVO user) {
        LoginUserContext.user.set(user);
    }
}
