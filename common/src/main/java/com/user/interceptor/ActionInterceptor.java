package com.user.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.user.constant.Constant;
import com.user.pojo.vo.common.ResponseVO;
import com.user.pojo.vo.login.UserLoginRespVO;
import com.user.utils.LoginUserContext;
import com.user.utils.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
@Slf4j
public class ActionInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        if (request.getServerPort() == 8084) {
            Map<String, String> users = redisService.bashHashGet(Constant.Role.CACHE);
            UserLoginRespVO userLoginRespVO = LoginUserContext.getUser();
            String roleStr = users.get(String.valueOf(userLoginRespVO.getUserId()));
            if (2 == Integer.parseInt(roleStr)) {
                return true;
            } else {
                log.info("操作被拦截");
                response.setStatus(HttpStatus.OK.value());
                ResponseVO responseVO = ResponseVO.noPermission();
                response.setContentType("application/json;charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(JSONObject.toJSON(responseVO));
                return false;
            }
        }
        return true;
    }
}
