package com.user.pojo.db;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserDO {
    private Long userId;
    private String name;
    private String phoneNumber;
    private String password;
    private Timestamp loginTime;
}
