package com.user.pojo.db;

import lombok.Data;

@Data
public class ProductDO {
    private Long productId;
    private String productName;
    private String productDesc;
    private String imgAddr;
    private String price;
    private String validity;
    private Integer enableStatus;
}
