package com.user.pojo.db;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class InstanceDO {
    private Long instanceId;
    private Long orderId;
    private Long productId;
    private Integer instanceState;
    private Timestamp createTime;
    private Timestamp effectiveTime;
    private Timestamp expireTime;
}
