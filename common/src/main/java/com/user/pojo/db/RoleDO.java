package com.user.pojo.db;

import lombok.Data;


@Data
public class RoleDO {
    private String roleId;
    private String userId;
    private Integer role;
}
