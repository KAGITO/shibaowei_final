package com.user.pojo.db;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class OrderDO {
    private Long orderId;
    private Long userId;
    private Integer orderState;
    private String totalPrice;
    private String reviewer;
    private Timestamp createTime;
    private Timestamp lastEditTime;
    private UserDO user;
    private List<InstanceDO> instances;
}
