package com.user.pojo.vo.login;

import lombok.Data;

@Data
public class UserLoginReqVO {
    private String phoneNumber;
    private String password;
}
