package com.user.pojo.vo.order;

import com.user.pojo.vo.product.ProductVO;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class OrderVO {
    private Long orderId;
    private String userName;
    private Integer orderState;
    private String totalPrice;
    private String reviewer;
    private Timestamp createTime;
    private Timestamp lastEditTime;
    private List<ProductVO> products;
}
