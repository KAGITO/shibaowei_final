package com.user.pojo.vo.login;

import lombok.Data;

@Data
public class UserLoginRespVO {
    private Long userId;

    private String name;

    private String phoneNumber;

    private String token;

    private Integer role;
}
