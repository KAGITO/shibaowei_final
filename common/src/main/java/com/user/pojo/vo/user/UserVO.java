package com.user.pojo.vo.user;

import lombok.Data;

@Data
public class UserVO {
    private Long userId;
    private String name;
    private String phoneNumber;
}
