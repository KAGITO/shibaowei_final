package com.user.pojo.vo.product;

import lombok.Data;

@Data
public class ProductVO {
    private Long productId;
    private String productName;
    private String productDesc;
    private String imgAddr;
    private String price;
    private String validity;
}
