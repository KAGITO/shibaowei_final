package com.user.pojo.vo.order;

import lombok.Data;

import java.util.List;

@Data
public class BaseOrderVO {
    private String userId;
    private List<String> productIds;
    private String totalPrice;
}
