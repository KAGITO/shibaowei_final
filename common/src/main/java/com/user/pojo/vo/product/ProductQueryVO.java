package com.user.pojo.vo.product;

import lombok.Data;

@Data
public class ProductQueryVO {
    private String keyword;
}
