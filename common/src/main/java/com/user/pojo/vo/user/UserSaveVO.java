package com.user.pojo.vo.user;


import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class UserSaveVO {
    private String name;
    private String phoneNumber;
    private String password;
}
