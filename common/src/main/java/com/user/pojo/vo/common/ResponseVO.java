package com.user.pojo.vo.common;

import lombok.Data;

@Data
public class ResponseVO<M> {

    private Integer code;   // 业务编号
    private String message; // 异常信息
    private M data;         // 业务数据返回

    private ResponseVO() {
    }

    // 成功但是无参数
    public static ResponseVO success() {
        ResponseVO response = new ResponseVO();
        response.setCode(200);
        response.setMessage("");
        return response;
    }

    // 成功有参数
    public static <M> ResponseVO success(M data) {
        ResponseVO response = new ResponseVO();
        response.setCode(200);
        response.setMessage("");
        response.setData(data);
        return response;
    }

    // 失败有参数
    public static <M> ResponseVO fail(M data) {
        ResponseVO response = new ResponseVO();
        response.setCode(500);
        response.setMessage("");
        response.setData(data);
        return response;
    }

    // 未登录异常
    public static <M> ResponseVO noLogin() {
        ResponseVO response = new ResponseVO();
        response.setCode(401);
        response.setMessage("请登录");
        return response;
    }

    public static <M> ResponseVO noPermission() {
        ResponseVO response = new ResponseVO();
        response.setCode(402);
        response.setMessage("不是管理员用户");
        return response;
    }


}