package com.user.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.InputStream;

@Data
@AllArgsConstructor
public class ImageHolder {
    private String imageName;
    private InputStream image;
}
